export class SettingsService{
  private settings = {};

  set(key: string, value: any){
    this.settings[key] = value;
  }

  get(key: string){
    let value: any = false;
    if(this.settings[key]){
      value = this.settings[key];
    }

    return value;
  }
}